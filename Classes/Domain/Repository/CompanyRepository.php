<?php
namespace Artif\ArtifCompanydatabase\Domain\Repository;

    /***
     *
     * This file is part of the "Company database" Extension for TYPO3 CMS.
     *
     * For the full copyright and license information, please read the
     * LICENSE.txt file that was distributed with this source code.
     *
     *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>
     *
     ***/
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * The repository for Companies
 */
class CompanyRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'companyName' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

//    /**
//     * @param $userUid
//     * @return array|QueryResultInterface
//     */
//    public function findByUser($userUid)
//    {
//        $query = $this->createQuery();
//        $settings = $query->getQuerySettings();
//        $settings->setRespectStoragePage(false);
//        $query->setQuerySettings($settings);
//        $query->matching($query->logicalAnd(
//            $query->equals(
//                'user', $userUid
//            )
//        ));
//        DebuggerUtility::var_dump($userUid);
//        DebuggerUtility::var_dump($query->execute());
//        die;
////        $queryParser = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\Storage\Typo3DbQueryParser::class);
//        return $query->execute()->getFirst();
//    }


    /**
     * @param $arguments
     * @return array|QueryResultInterface
     * @internal param $keyword
     */
    public function findSearchTerm($arguments)
    {
        $query = $this->createQuery();

        if ($arguments['sectors']) {
            foreach ($arguments['sectors'] as $sector) {
                $constraints[] = $query->contains('sectors', $sector);
            }
        }
        if ($arguments['focuses']) {
            foreach ($arguments['focuses'] as $focus) {
                $constraints[] = $query->contains('focuses', $focus);
            }
        }
        if ($arguments['searchterm']) {
            $constraints[] = $query->like('company_name', '%' . $arguments['searchterm'] . '%');
        }

        if ($constraints) {
            $query->matching($query->logicalAnd($constraints));
        }

        return $query->execute();
    }

}
