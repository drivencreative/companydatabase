<?php
namespace Artif\ArtifCompanydatabase\Domain\Model;

    /***
     *
     * This file is part of the "Company database" Extension for TYPO3 CMS.
     *
     * For the full copyright and license information, please read the
     * LICENSE.txt file that was distributed with this source code.
     *
     *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>
     *
     ***/
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Company
 */
class Company extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * tstamp
     *
     * @var \DateTime
     */
    protected $tstamp = null;

    /**
     * crdate
     *
     * @var \DateTime
     */
    protected $crdate = null;

    /**
     * companyName
     *
     * @var string
     */
    protected $companyName = '';

    /**
     * profile
     *
     * @var string
     */
    protected $profile = '';

    /**
     * companyDescription
     *
     * @var string
     */
    protected $companyDescription = '';

    /**
     * homepage
     *
     * @var string
     */
    protected $homepage = '';

    /**
     * photo
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $photo = null;

    /**
     * logo
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $logo = null;

    /**
     * companyContactPublic
     *
     * @var \Artif\ArtifCompanydatabase\Domain\Model\Contact
     */
    protected $companyContactPublic = null;

    /**
     * contactDetailsPrivate
     *
     * @var \Artif\ArtifCompanydatabase\Domain\Model\Contact
     */
    protected $contactDetailsPrivate = null;

    /**
     * focuses
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifCompanydatabase\Domain\Model\Focus>
     */
    protected $focuses = null;

    /**
     * sectors
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifCompanydatabase\Domain\Model\Sector>
     */
    protected $sectors = null;

    /**
     * other
     *
     * @var string
     */
    protected $other = null;

    /**
     * user
     *
     * @var \Artif\ArtifCompanydatabase\Domain\Model\FrontendUser
     * @lazy
     * @cascade remove
     */
    protected $user = null;

    /**
     * hidden
     *
     * @var \integer
     */
    protected $hidden;

    /**
     * terms
     *
     * @var \integer
     */
    protected $terms;

    /**
     * cdate
     *
     * @var \integer
     */
    protected $cdate;

    /**
     * status
     *
     * @var \integer
     */
    protected $status;

    /**
     * reminder
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifCompanydatabase\Domain\Model\Reminder>
     * @lazy
     * @cascade remove
     */
    protected $reminder = null;

    /**
     * Returns the hidden
     *
     * @return \integer $hidden
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Sets the hidden
     *
     * @param \integer $hidden
     * @return void
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * Returns the terms
     *
     * @return \integer $terms
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * Sets the terms
     *
     * @param \integer $terms
     * @return void
     */
    public function setTerms($terms)
    {
        $this->terms = $terms;
    }

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->focuses = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->sectors = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->reminder = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the companyName
     *
     * @return string $companyName
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Sets the companyName
     *
     * @param string $companyName
     * @return void
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * Returns the companyDescription
     *
     * @return string $companyDescription
     */
    public function getCompanyDescription()
    {
        return $this->companyDescription;
    }

    /**
     * Sets the companyDescription
     *
     * @param string $companyDescription
     * @return void
     */
    public function setCompanyDescription($companyDescription)
    {
        $this->companyDescription = $companyDescription;
    }

    /**
     * Returns the homepage
     *
     * @return string $homepage
     */
    public function getHomepage()
    {
        return $this->homepage;
    }

    /**
     * Sets the homepage
     *
     * @param string $homepage
     * @return void
     */
    public function setHomepage($homepage)
    {
        $this->homepage = $homepage;
    }

    /**
     * Returns the photo
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $photo
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Sets the photo
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $photo
     * @return void
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    /**
     * Returns the logo
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $logo
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Sets the logo
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $logo
     * @return void
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * Adds a Focus
     *
     * @param \Artif\ArtifCompanydatabase\Domain\Model\Focus $focus
     * @return void
     */
    public function addFocus(\Artif\ArtifCompanydatabase\Domain\Model\Focus $focus)
    {
        $this->focuses->attach($focus);
    }

    /**
     * Removes a Focus
     *
     * @param \Artif\ArtifCompanydatabase\Domain\Model\Focus $focusToRemove The Focus to be removed
     * @return void
     */
    public function removeFocus(\Artif\ArtifCompanydatabase\Domain\Model\Focus $focusToRemove)
    {
        $this->focuses->detach($focusToRemove);
    }

    /**
     * Returns the focuses
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifCompanydatabase\Domain\Model\Focus> $focuses
     */
    public function getFocuses()
    {
        return $this->focuses;
    }

    /**
     * Sets the focuses
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifCompanydatabase\Domain\Model\Focus> $focuses
     * @return void
     */
    public function setFocuses(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $focuses)
    {
        $this->focuses = $focuses;
    }

    /**
     * Adds a Sector
     *
     * @param \Artif\ArtifCompanydatabase\Domain\Model\Sector $sector
     * @return void
     */
    public function addSector(\Artif\ArtifCompanydatabase\Domain\Model\Sector $sector)
    {
        $this->sectors->attach($sector);
    }

    /**
     * Removes a Sector
     *
     * @param \Artif\ArtifCompanydatabase\Domain\Model\Sector $sectorToRemove The Sector to be removed
     * @return void
     */
    public function removeSector(\Artif\ArtifCompanydatabase\Domain\Model\Sector $sectorToRemove)
    {
        $this->sectors->detach($sectorToRemove);
    }

    /**
     * Returns the sectors
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifCompanydatabase\Domain\Model\Sector> $sectors
     */
    public function getSectors()
    {
        return $this->sectors;
    }

    /**
     * Sets the sectors
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifCompanydatabase\Domain\Model\Sector> $sectors
     * @return void
     */
    public function setSectors(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $sectors)
    {
        $this->sectors = $sectors;
    }

    /**
     * Returns the user
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Sets the user
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $user
     * @return void
     */
    public function setUser(\TYPO3\CMS\Extbase\Domain\Model\FrontendUser $user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getCdate()
    {
        return $this->cdate;
    }

    /**
     * @param int $cdate
     */
    public function setCdate($cdate)
    {
        $this->cdate = $cdate;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return Contact
     */
    public function getContactDetailsPrivate(): Contact
    {
        return $this->contactDetailsPrivate;
    }

    /**
     * @param Contact $contactDetailsPrivate
     */
    public function setContactDetailsPrivate(Contact $contactDetailsPrivate)
    {
        $this->contactDetailsPrivate = $contactDetailsPrivate;
    }

    /**
     * @return Contact
     */
    public function getCompanyContactPublic(): Contact
    {
        return $this->companyContactPublic;
    }

    /**
     * @param Contact $companyContactPublic
     */
    public function setCompanyContactPublic(Contact $companyContactPublic)
    {
        $this->companyContactPublic = $companyContactPublic;
    }

    /**
     * @return string
     */
    public function getProfile (): string
    {
        return $this->profile;
    }

    /**
     * @param string $profile
     */
    public function setProfile (string $profile)
    {
        $this->profile = $profile;
    }

    /**
     * @return \DateTime
     */
    public function getTstamp ()
    {
        return $this->tstamp;
    }

    /**
     * @param \DateTime $tstamp
     */
    public function setTstamp (\DateTime $tstamp = null)
    {
        $this->tstamp = $tstamp;
    }

    /**
     * @return \DateTime
     */
    public function getCrdate ()
    {
        return $this->crdate;
    }

    /**
     * @param \DateTime $crdate
     */
    public function setCrdate (\DateTime $crdate = null)
    {
        $this->crdate = $crdate;
    }

    /**
     * Returns the reminder
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifCompanydatabase\Domain\Model\Reminder> $reminder
     */
    public function getReminder()
    {
        return $this->reminder;
    }

    /**
     * Sets the reminder
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifCompanydatabase\Domain\Model\Reminder> $reminder
     * @return void
     */
    public function setReminder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $reminder)
    {
        $this->reminder = $reminder;
    }

    /**
     * Adds a Reminder
     *
     * @param \Artif\ArtifCompanydatabase\Domain\Model\Reminder $reminder
     * @return void
     */
    public function addReminder(\Artif\ArtifCompanydatabase\Domain\Model\Reminder $reminder)
    {
        $this->reminder->attach($reminder);
    }

    /**
     * Removes a Reminder
     *
     * @param \Artif\ArtifCompanydatabase\Domain\Model\Reminder $reminderToRemove The Reminder to be removed
     * @return void
     */
    public function removeReminder(\Artif\ArtifCompanydatabase\Domain\Model\Reminder $reminderToRemove)
    {
        $this->reminder->detach($reminderToRemove);
    }

    /**
     * @return string
     */
    public function getOther()
    {
        return $this->other;
    }

    /**
     * @param string $other
     */
    public function setOther($other)
    {
        $this->other = $other;
    }

}
