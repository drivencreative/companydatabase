<?php

namespace Artif\ArtifCompanydatabase\Domain\Model;

/***
 *
 * This file is part of the "Company database" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <info@artif.com>
 *
 ***/
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Class Contact
 * @package Artif\ArtifCompanydatabase\Domain\Model
 */
class Contact extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * salutation
     *
     * @var string
     */
    protected $salutation = '';

    /**
     * firstName
     *
     * @var string
     */
    protected $firstName = '';

    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * phone
     *
     * @var string
     */
    protected $phone = '';

    /**
     * mobile
     *
     * @var string
     */
    protected $mobile = '';

    /**
     * email
     *
     * @var string
     */
    protected $email = '';

    /**
     * street
     *
     * @var string
     */
    protected $street = '';

    /**
     * zip
     *
     * @var string
     */
    protected $zip = '';

    /**
     * city
     *
     * @var string
     */
    protected $city = '';

    /**
     * @param string $salutation
     */
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;
    }

    /**
     * @return string
     */
    public function getSalutation()
    {
        return $this->salutation;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return LocalizationUtility::translate('salutation.'.$this->salutation, 'ArtifCompanydatabase');
    }

    /**
     * @return string
     */
    public function getSalutationText()
    {
        return LocalizationUtility::translate('salutation.text.'.$this->salutation, 'ArtifCompanydatabase');
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }
}