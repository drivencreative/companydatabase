<?php
namespace Artif\ArtifCompanydatabase\Domain\Model;

/***
 *
 * This file is part of the "Company database" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>
 *
 ***/

/**
 * Class FrontendUser
 * @package Artif\ArtifCompanydatabase\Domain\Model
 */
class FrontendUser extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
{
    /**
     * disable
     *
     * @var \integer
     */
    protected $disable;

    /**
     * crdate
     *
     * @var \DateTime
     */
    protected $crdate = null;

    /**
     * Returns the disable
     *
     * @return \integer $disable
     */
    public function getDisable() {
        return $this->disable;
    }

    /**
     * Sets the disable
     *
     * @param \integer $disable
     * @return void
     */
    public function setDisable($disable) {
        $this->disable = $disable;
    }

    /**
     * @return \DateTime
     */
    public function getCrdate ()
    {
        return $this->crdate;
    }

    /**
     * @param \DateTime $crdate
     */
    public function setCrdate (\DateTime $crdate = null)
    {
        $this->crdate = $crdate;
    }

}