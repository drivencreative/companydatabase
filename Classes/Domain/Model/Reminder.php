<?php
namespace Artif\ArtifCompanydatabase\Domain\Model;

/***
 *
 * This file is part of the "Company database" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>
 *
 ***/

/**
 * Reminder
 */
class Reminder extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * type
     *
     * @var string
     */
    protected $type = '';

    /**
     * date
     *
     * @var \DateTime
     */
    protected $date = null;

    /**
     * @return string
     */
    public function getType ()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType (string $type)
    {
        $this->type = $type;
    }

    /**
     * @return \DateTime
     */
    public function getDate ()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate (\DateTime $date = null)
    {
        $this->date = $date;
    }

}
