<?php
namespace Artif\ArtifCompanydatabase\ViewHelpers;

/**
 * Class CssToInlineViewHelper
 *
 * @package Artif\ArtifCompanydatabase\ViewHelpers
 */
class CssToInlineViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * @param string $content
     * @param string $css
     * @return string
     */
    public function render($content = NULL, $css = '')
    {
        if ($content === NULL) {
            $content = $this->renderChildren();
        }
        $cssToInline = new \Artif\ArtifCompanydatabase\System\CssToInline($content, ($css ? file_get_contents($css) : ''));
        return $cssToInline->emogrify();
    }

}
