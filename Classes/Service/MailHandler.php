<?php
namespace Artif\ArtifCompanydatabase\Service;

/***
 *
 * This file is part of the "Company database" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>
 *
 ***/
use Artif\ArtifCompanydatabase\System\Utility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * MailHandler
 */
class MailHandler
{

    /**
     * View
     *
     * @var \Artif\ArtifCompanydatabase\View\HtmlView
     * @inject
     */
    protected $view = NULL;

    /**
     * @var string
     **/
    protected $subject = '';

    /**
     * @var string
     **/
    protected $sender = '';

    /**
     * @var string
     **/
    protected $receiver = '';

    /**
     * @var string
     **/
    protected $content = '';

    /**
     * @var string
     **/
    protected $attachment = null;

    /**
     * @param string $subject
     *
     * @return MailHandler
     */
    public function setSubject (string $subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @param string $sender
     *
     * @return MailHandler
     */
    public function setSender (string $sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * @param string $receiver
     *
     * @return MailHandler
     */
    public function setReceiver (string $receiver)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * @param string $content
     * @param array $variables
     *
     * @return MailHandler
     */
    public function setContent (string $content, array $variables = [])
    {
        $this->content = $content;

        if ($variables) {
            $this->view->assignMultiple($variables);

            $dom = GeneralUtility::makeInstance(\DOMDocument::class);
            $dom->loadHTML(
                mb_convert_encoding(
                    GeneralUtility::makeInstance(ContentObjectRenderer::class)->parseFunc($content, [], '< lib.parseFunc_RTE'),
                    'HTML-ENTITIES',
                    'UTF-8'
                ),
                LIBXML_HTML_NODEFDTD
            );
            $this->content = $this->view->renderHtml($dom->saveHTML());
        }

        return $this;
    }

    /**
     * @param array $variables
     *
     * @return MailHandler
     */
    public function assignVariables (array $variables)
    {

        return $this;
    }

    /**
     * @param string|array $attachment
     */
    public function setAttachment ($attachment)
    {
        $this->attachment = $attachment;
    }

    /**
     * Send
     *
     * @return void
     */
    public function send() {
        return Utility::sendEmail(
            $this->receiver,
            $this->sender,
            $this->subject,
            $this->content,
            $this->attachment
        );
    }

}
