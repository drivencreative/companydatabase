<?php

namespace Artif\ArtifCompanydatabase\Service;


class AccessControlService implements \TYPO3\CMS\Core\SingletonInterface
{
    /**
     * Do we have a logged in feuser
     * @return boolean
     */
    public static function hasLoggedInFrontendUser() {
        return $GLOBALS['TSFE']->loginUser == 1 ? TRUE : FALSE;
    }

    /**
     * Get the uid of the current feuser
     * @return mixed
     */
    public static function getFrontendUserUid() {
        if (self::hasLoggedInFrontendUser() && !empty($GLOBALS['TSFE']->fe_user->user['uid'])) {
            return intval($GLOBALS['TSFE']->fe_user->user['uid']);
        }
        return NULL;
    }

    /**
     * @param \Artif\ArtifCompanydatabase\Domain\Model\FrontendUser $user
     * @return boolean
     */
    public static function isAccessAllowed(\Artif\ArtifCompanydatabase\Domain\Model\FrontendUser $user) {
        return self::getFrontendUserUid() === $user->getUid() ? TRUE : FALSE;
    }
}