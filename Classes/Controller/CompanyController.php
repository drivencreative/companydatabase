<?php

namespace Artif\ArtifCompanydatabase\Controller;

/***
 *
 * This file is part of the "Company database" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>
 *
 ***/
use Artif\ArtifCompanydatabase\Authentication\StandardLogin;
use Artif\ArtifCompanydatabase\Domain\Model\Company;
use Artif\ArtifCompanydatabase\Domain\Model\FrontendUser;
use Artif\ArtifCompanydatabase\Property\TypeConverter\UploadedFileReferenceConverter;
use Artif\ArtifCompanydatabase\Service\AccessControlService;
use Artif\ArtifCompanydatabase\System\Utility;
use Artif\ArtifCompanydatabase\View\HtmlView;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Extbase\Property\PropertyMappingConfiguration;

/**
 * CompanyController
 */
class CompanyController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * companyRepository
     *
     * @var \Artif\ArtifCompanydatabase\Domain\Repository\CompanyRepository
     * @inject
     */
    protected $companyRepository = null;

    /**
     * companyRepository
     *
     * @var \Artif\ArtifCompanydatabase\Domain\Repository\FocusRepository
     * @inject
     */
    protected $focusRepository = null;

    /**
     * companyRepository
     *
     * @var \Artif\ArtifCompanydatabase\Domain\Repository\SectorRepository
     * @inject
     */
    protected $sectorRepository = null;

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository;

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserGroupRepository
     * @inject
     */
    protected $frontendUserGroupRepository;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
     * @inject
     */
    protected $persistenceManager;

    /**
     * UriBuilder
     *
     * @var \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder
     * @inject
     */
    protected $uriBuilder = NULL;

    /**
     * MailHandler
     *
     * @var \Artif\ArtifCompanydatabase\Service\MailHandler
     * @inject
     */
    protected $mailHandler = null;

    /**
     * action show
     *
     * @return void
     */
    public function indexAction()
    {
        if (AccessControlService::hasLoggedInFrontendUser()) {
            // TODO: already loggedin
            $company = $this->companyRepository->findOneByUser(AccessControlService::getFrontendUserUid());
            if ($company) {
                $this->redirect('show', null, null, ['company' => $company]);
            }
        }
    }

    /**
     * action show
     *
     * @return void
     */
    public function loginAction()
    {

        if (isset($this->request->getArguments()['login'])) {
            //TODO: login user
            if (!AccessControlService::hasLoggedInFrontendUser()) {
                // login user:
                /** @var StandardLogin $standardLogin */
                $standardLogin = $this->objectManager->get(StandardLogin::class);
                if (isset($this->request->getArguments()['username']) && isset($this->request->getArguments()['password']) && $standardLogin->login($this->request->getArguments()['username'], $this->request->getArguments()['password'])) {
                    $userCompany = $this->companyRepository->findOneByUser(AccessControlService::getFrontendUserUid());
                    if ($userCompany)
                        $this->redirect('edit', null, null, ['company' => $userCompany]);
                    else
                        $this->redirect('index');
                } else {
                    $this->addFlashMessage(LocalizationUtility::translate('incorectUser', 'artif_companydatabase'), '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
                    $this->redirect('index');
                }
            } else {
                // redirect logedin user to index
                $this->redirect('list');
            }
        } else if (isset($this->request->getArguments()['new'])) {
            $this->redirect('new');
        } else {
            $this->redirect('index');
        }
    }

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $args = $this->request->getArguments();
        $focuses = $this->focusRepository->findAll();
        $sectors = $this->sectorRepository->findAll();

        $this->view->assign('focuses', $focuses);
        $this->view->assign('sectors', $sectors);

        $this->controllerContext->getFlashMessageQueue()->getAllMessagesAndFlush();

        if (isset($args['search'])) {
            $searchterm = str_replace(' ', '', $args['searchterm']);
            $this->view->assign('searchterm', $args['searchterm']);
            $this->view->assign('selectedFocus', $args['focuses']);
            $this->view->assign('selectedSector', $args['sectors']);

            if (!$searchterm && (!$args['focuses'] || ($focuses->count() == count($args['focuses']))) && (!$args['sectors'] || ($sectors->count() == count($args['sectors'])))) {
                $this->addFlashMessage(LocalizationUtility::translate('search_validatation', 'artif_companydatabase'), '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
            } elseif ($searchterm && strlen($searchterm) < 3) {
                $this->addFlashMessage(LocalizationUtility::translate('shortSearch', 'artif_companydatabase'), '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
            } else {
                // All selected options are combined with "OR", same as if empty
                if ($focuses->count() == count($args['focuses'])) {
                    unset($args['focuses']);
                }
                if ($sectors->count() == count($args['sectors'])) {
                    unset($args['sectors']);
                }
            }
        }
        $this->view->assign('companies', $this->companyRepository->findSearchTerm($args));

        /** Settings from registration plugin **/
        $typoscript = $this->objectManager->get(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::class)->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
        $plugin = $typoscript['plugin.']['tx_artifcompanydatabase_registration.']['settings.']['scheduler.'];
        $data = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('pi_flexform', 'tt_content', 'uid='.$plugin['plugin']);
        $settings = $this->objectManager->get(\TYPO3\CMS\Extbase\Service\FlexFormService::class)->convertFlexFormContentToArray($data['pi_flexform'])['settings'];
        $this->view->assign('settings', $settings);
    }

    /**
     * action show
     *
     * @param Company $company
     * @return void
     */
    public function showAction(Company $company)
    {

        // searchterm for back action
        if (isset($this->request->getArguments()['searchterm']) && strlen($this->request->getArguments()['searchterm']) >= 3) {
            $this->view->assign('searchterm', $this->request->getArguments()['searchterm']);
            $this->view->assign('selectedFocus', $this->request->getArguments()['selectedFocus']);
            $this->view->assign('selectedSector', $this->request->getArguments()['selectedSector']);
        }

        if (AccessControlService::getFrontendUserUid()) {
            if ($company->getUser()->getUid() == AccessControlService::hasLoggedInFrontendUser())
                $this->view->assign('edit', 1);
        }
//        if($_SERVER['REMOTE_ADDR'] == '178.222.100.22'){
//            DebuggerUtility::var_dump($company->getCompanyContactPublic()->getSalutationText());die;
//        }
        $this->view->assign('company', $company);
    }

    /**
     * action new
     *
     * @return void
     */
    public function newAction()
    {
        // set session key "new" checked in createAction
        $GLOBALS['TSFE']->fe_user->setKey('ses', 'new', time());
        $GLOBALS['TSFE']->fe_user->storeSessionData();

        $this->view->assign('focuses', $this->focusRepository->findAll());
        $this->view->assign('sectors', $this->sectorRepository->findAll());
        $this->view->assign('new', true);

        if ($this->request->getOriginalRequest()) {
            $this->view->assign('company', $this->request->getOriginalRequest()->getArgument('newCompany'));
        }
    }


//    public function initializeAction()
//    {
//        parent::initializeAction(); // TODO: Change the autogenerated stub
//    }


    /**
     * Initialize Create Action
     */
    public function initializeCreateAction()
    {
        /** @var $configuration \TYPO3\CMS\Extbase\Property\PropertyMappingConfiguration */
        $configuration = reset($this->arguments)->getPropertyMappingConfiguration();
        $configuration
            ->skipUnknownProperties()
            ->allowAllProperties()
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                true
            )
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                true
            );
    }

    /**
     * action create
     * If the Validator is not a Domain Model Validator or it's not placed in Domain\Model\Validator it's executed once
     * @param Company $newCompany
     * @validate $newCompany \Artif\ArtifCompanydatabase\Validator\CompanyValidator
     * @return void
     */
    public function createAction(Company $newCompany)
    {
        $reminder = new \Artif\ArtifCompanydatabase\Domain\Model\Reminder();
        $reminder->setType('reminderHalfYear');
        $reminder->setDate((new \DateTime())->modify('+6  months'));
        $newCompany->addReminder($reminder);

        $reminder = new \Artif\ArtifCompanydatabase\Domain\Model\Reminder();
        $reminder->setType('reminderOneYear');
        $reminder->setDate((new \DateTime())->modify('+12  months'));
        $newCompany->addReminder($reminder);

        $reminder = new \Artif\ArtifCompanydatabase\Domain\Model\Reminder();
        $reminder->setType('deletion');
        $reminder->setDate((new \DateTime())->modify('+13  months'));
        $newCompany->addReminder($reminder);

        // if missing sessionkey "new" don't allow company creation
        $sessionData = $GLOBALS['TSFE']->fe_user->getKey('ses', 'new');
//        if ($sessionData != null && $sessionData != 'done') {
        $arguments = $this->request->getArguments();
        if ($arguments['focuses']) {
            foreach ($arguments['focuses'] as $focus) {
                $newCompany->addFocus($this->focusRepository->findByUid($focus));
            }
        }
        if ($arguments['sectors']) {
            foreach ($arguments['sectors'] as $sector) {
                $newCompany->addSector($this->sectorRepository->findByUid($sector));
            }
        }
        $frontendUser = new FrontendUser();
        $frontendUser->setUsername($arguments['newCompany']['user']['username']);
        if (\TYPO3\CMS\Saltedpasswords\Utility\SaltedPasswordsUtility::isUsageEnabled('FE')) {
            $objSalt = \TYPO3\CMS\Saltedpasswords\Salt\SaltFactory::getSaltingInstance(NULL);
            if (is_object($objSalt)) {
                $saltedPassword = $objSalt->getHashedPassword($arguments['newCompany']['user']['password']);
            }
        }


        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);

        /** @var \TYPO3\CMS\Extensionmanager\Utility\ConfigurationUtility $configurationUtility */
        $configurationUtility = $objectManager->get(\TYPO3\CMS\Extensionmanager\Utility\ConfigurationUtility::class);
        $extensionConfiguration = $configurationUtility->getCurrentConfiguration('artif_publications');

        //todo: set group uid from settings
        if (isset($this->settings['userGroupUid']))
            $groupUid = $this->settings['userGroupUid'];
        else if (isset($extensionConfiguration['userGroupUid']['value']))
            $groupUid = $extensionConfiguration['userGroupUid']['value'];
        else {
            \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump("No user group");
            die;
        }
        $usergroup = $this->frontendUserGroupRepository->findByUid($groupUid);
        $frontendUser->setPassword($saltedPassword);
        $frontendUser->setDisable(1);
        $frontendUser->addUsergroup($usergroup);
        $this->frontendUserRepository->add($frontendUser);

        $newCompany->setUser($frontendUser);
        $newCompany->setCdate(time());
        $newCompany->setHidden(1);

        $this->companyRepository->add($newCompany);
        // change creation status key 'new' to prevent multiple creation
        $GLOBALS['TSFE']->fe_user->setKey('ses', 'new', 'done');
        $GLOBALS['TSFE']->fe_user->storeSessionData();

        $this->persistenceManager->persistAll();

        if ($this->settings['adminEmail']) {
            $adminEmail = $this->settings['adminEmail'];
        } else if (isset($extensionConfiguration['adminEmail']['value'])) {
            $groupUid = $extensionConfiguration['adminEmail']['value'];
        } else {
            \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump("No admin email");
            die;
        }

        if ($this->settings['registration']['subject'] && $this->settings['registration']['content']) {
            $link = $this->linkGenerator($newCompany, 'adminReview');
            $this->mailWrapper(
                $newCompany,
                $link,
                $newCompany->getContactDetailsPrivate()->getEmail(),
                $this->settings['registration']['subject'],
                $this->settings['registration']['content']
            );
        }

        $this->redirect('confirmation');
//        } else $this->redirect('list');
    }

    /**
     * initializeAdminReview
     */
    public function initializeAdminReviewAction()
    {
        unset(
            $GLOBALS['TCA']['tx_artifcompanydatabase_domain_model_company']['ctrl']['enablecolumns']['disabled'],
            $GLOBALS['TCA']['fe_users']['ctrl']['enablecolumns']['disabled']
        );
    }

    /**
     * adminReview action send email for admin review page
     * @param Company $company
     */
    public function adminReviewAction(Company $company)
    {
        if ($this->settings['newPost']['subject'] && $this->settings['newPost']['content']) {
            $link = $this->linkGenerator($company, 'manageCompany');
            $this->mailWrapper(
                $company,
                $link,
                $this->settings['adminEmail'],
                $this->settings['newPost']['subject'],
                $this->settings['newPost']['content']
            );
        }
    }

    /**
     * action initializeManageCompany
     */
    public function initializeManageCompanyAction()
    {
        unset(
            $GLOBALS['TCA']['tx_artifcompanydatabase_domain_model_company']['ctrl']['enablecolumns']['disabled'],
            $GLOBALS['TCA']['fe_users']['ctrl']['enablecolumns']['disabled']
        );
    }

    /**
     * action manageCompany Manage company actions view
     * @param Company $company
     */
    public function manageCompanyAction(Company $company)
    {
        $this->view->assign('company', $company);
    }

    /**
     * action initializeSwitcher
     */
    public function initializeSwitcherAction()
    {
        unset(
            $GLOBALS['TCA']['tx_artifcompanydatabase_domain_model_company']['ctrl']['enablecolumns']['disabled'],
            $GLOBALS['TCA']['fe_users']['ctrl']['enablecolumns']['disabled']
        );
    }

    /**
     * action manageCompany Trigger active or delete
     * @param Company $company
     */
    public function switcherAction(Company $company)
    {
        $trigger = $this->request->getArguments()['active'];
        if ($trigger) {
            $company->setHidden(0);
            /** @var FrontendUser $feUser */
            $feUser = $company->getUser();
            $feUser->setDisable(0);
            $this->frontendUserRepository->update($feUser);
            $this->companyRepository->update($company);

            if ($this->settings['activation']['subject'] && $this->settings['activation']['content']) {
                $link = $this->linkGenerator($company, 'edit');
                $this->mailWrapper(
                    $company,
                    $link,
                    $company->getContactDetailsPrivate()->getEmail(),
                    $this->settings['activation']['subject'],
                    $this->settings['activation']['content']
                );
            }
        } else {
            $this->companyRepository->remove($company);

            if ($this->settings['reject']['subject'] && $this->settings['reject']['content']) {
                $this->mailWrapper(
                    $company,
                    '',
                    $company->getContactDetailsPrivate()->getEmail(),
                    $this->settings['reject']['subject'],
                    $this->settings['reject']['content']
                );
            }
        }

        $this->view->assign('company', $company);
    }

    /**
     * action confirmation
     */
    public function confirmationAction()
    {
        $GLOBALS['TSFE']->fe_user->setKey('ses', 'new', null);
        $GLOBALS['TSFE']->fe_user->storeSessionData();
    }

    /**
     * action initializeEdit
     */
    public function initializeEditAction()
    {
        unset(
            $GLOBALS['TCA']['tx_artifcompanydatabase_domain_model_company']['ctrl']['enablecolumns']['disabled'],
            $GLOBALS['TCA']['fe_users']['ctrl']['enablecolumns']['disabled']
        );
    }

    /**
     * action edit
     *
     * @param Company $company
     * @return void
     */
    public function editAction(Company $company)
    {
        $this->controllerContext->getFlashMessageQueue()->getAllMessagesAndFlush();

        if (AccessControlService::hasLoggedInFrontendUser()) {
            $userCompany = $this->companyRepository->findOneByUser(AccessControlService::getFrontendUserUid());
            if (!$userCompany || $company->getUid() != $userCompany->getUid())
                $this->redirect('list');
            else {
                $this->view->assign('focuses', $this->focusRepository->findAll());
                $this->view->assign('sectors', $this->sectorRepository->findAll());
                $this->view->assign('company', $company);
            }
        } else {
            $this->redirect('login');
        }
    }

    /**
     * Set TypeConverter option for image upload
     */
    public function initializeUpdateAction()
    {
        $this->setTypeConverterConfigurationForImageUpload('company');
        unset(
            $GLOBALS['TCA']['tx_artifcompanydatabase_domain_model_company']['ctrl']['enablecolumns']['disabled'],
            $GLOBALS['TCA']['fe_users']['ctrl']['enablecolumns']['disabled']
        );
    }

    /**
     * action update
     *
     * @param Company $company
     * @validate $company \Artif\ArtifCompanydatabase\Validator\CompanyValidator
     * @return void
     */
    public function updateAction(Company $company)
    {
        if (AccessControlService::hasLoggedInFrontendUser()) {
            $userCompany = $this->companyRepository->findOneByUser(AccessControlService::getFrontendUserUid());
            if (!$userCompany || $company->getUid() != $userCompany->getUid())
                $this->redirect('list');
            else {
                $this->companyRepository->update($company);
                $this->persistenceManager->persistAll();

                $bodyMessage = Utility::getStandaloneView(['company' => $company, 'settings' => $this->settings], 'Partials/Mails/CompanyUpdate.html');

                $this->mailHandler
                    ->setReceiver($this->settings['adminEmail'])
                    ->setSender($this->settings['senderEmail'])
                    ->setSubject(LocalizationUtility::translate('tx_artif_companydatabase_registration.companyUpdate.subject', 'artif_companydatabase', [$company->getUid()]))
                    ->setContent($bodyMessage)
                    ->send();


                $this->redirect('show', null, null, ['company' => $company]);
            }
        }
        if ($this->request->getArguments()['userUid']) {
            $company->setHidden(true);
            $this->companyRepository->update($company);
        }
        $this->redirect('list');
    }

    /**
     * action delete
     *
     * @param Company $company
     * @return void
     */
    public function deleteAction(Company $company)
    {
        if (AccessControlService::getFrontendUserUid() && $this->request->hasArgument('confirm')) {
            $userCompany = $this->companyRepository->findOneByUser(AccessControlService::getFrontendUserUid());
            if ($userCompany || $company->getUid() == $userCompany->getUid()) {
                $company->setHidden(1);
                /** @var FrontendUser $feUser */
                $feUser = $company->getUser();
                $feUser->setDisable(1);
                $this->frontendUserRepository->update($feUser);
                $this->companyRepository->update($company);

//                $this->companyRepository->remove($company);
            }
        }
        $this->redirect('index');
    }

    /**
     *  Logout user
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     */
    public function logoutAction()
    {
        if ($GLOBALS['TSFE']->loginUser && $GLOBALS['TSFE']->fe_user->user) {
            // login user:
            $standardLogin = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Artif\ArtifCompanydatabase\Authentication\StandardLogin::class);
            $standardLogin->logout();
        }
        $this->redirect('index');
    }

    /**
     *
     */
    protected function setTypeConverterConfigurationForImageUpload($argumentName)
    {
        $uploadConfiguration = [
            UploadedFileReferenceConverter::CONFIGURATION_ALLOWED_FILE_EXTENSIONS => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
            UploadedFileReferenceConverter::CONFIGURATION_UPLOAD_FOLDER => '1:/content/',
        ];
        /** @var PropertyMappingConfiguration $newExampleConfiguration */
        $newExampleConfiguration = $this->arguments[$argumentName]->getPropertyMappingConfiguration();

        $newExampleConfiguration->forProperty('photo')
            ->setTypeConverterOptions(
                \Artif\ArtifCompanydatabase\Property\TypeConverter\UploadedFileReferenceConverter::class,
                $uploadConfiguration
            );
    }

    /**
     * @param Company $newCompany
     * @param $action
     * @return string
     */
    public function linkGenerator(Company $newCompany, $action)
    {
        $link = $this->uriBuilder
            ->setCreateAbsoluteUri(true)
            ->setTargetPageUid($GLOBALS['TSFE']->page['uid'])
            ->setArguments(
                [
                    'tx_artifcompanydatabase_registration' =>
                        [
                            'action' => $action,
                            'company' => $newCompany
                        ]
                ]
            )
            ->buildFrontendUri();
        return $link;
    }

    /**
     * @param Company $company
     * @param string $link
     * @param string $receiver
     * @param $subject
     * @param $content
     */
    public function mailWrapper(Company $company, $link, $receiver, $subject, $content)
    {
        $this->mailHandler
            ->setReceiver($receiver)
            ->setSender($this->settings['senderEmail'])
            ->setSubject($subject)
            ->setContent($content, [
                'company' => $company,
                'link' => ($link ? "<a href='$link'>Link</a>" : '')
            ])
            ->send();
    }

}
