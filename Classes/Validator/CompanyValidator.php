<?php

namespace Artif\ArtifCompanydatabase\Validator;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;

//If the Validator is not a Domain Model Validator or it's not placed in Domain\Model\Validator it's executed once

class CompanyValidator extends \TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator
{
    /**
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository;

    public function isValid($company)
    {
        $arguments = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('tx_artifcompanydatabase_registration');
        $parameter = 'newCompany';
        if ($arguments['company']) {
            $parameter = 'company';
        }
//            DebuggerUtility::var_dump($_SERVER['REMOTE_ADDR']);die;
//        if ($_SERVER['REMOTE_ADDR'] = '178.222.100.22') {
//            DebuggerUtility::var_dump($this->integerValidator);
//        }

        $valid = true;
        if (empty($company->getCompanyName())) {
            $this->addError(LocalizationUtility::translate('missingCompanyName', 'artif_companydatabase'), 'companyName');
            $valid = false;
        }
        if (!isset($arguments[$parameter]['sectors']) || empty($arguments[$parameter]['sectors'])) {
            $this->addError(LocalizationUtility::translate('missingSector', 'artif_companydatabase'), 'sectors');
            $valid = false;
        }

        // ContactDetailsPrivate
        if (empty($company->getContactDetailsPrivate())) {
            $this->addError(LocalizationUtility::translate('missingContactDetailsPrivate', 'artif_companydatabase'), 'contactDetailsPrivate');
            $valid = false;
        }
        if(empty($company->getContactDetailsPrivate()->getName())){
            $this->addError(LocalizationUtility::translate('missingName', 'artif_companydatabase'), 'name');
            $valid = false;
        }
        if(empty($company->getContactDetailsPrivate()->getFirstName())){
            $this->addError(LocalizationUtility::translate('missingFirstName', 'artif_companydatabase'), 'firstName');
            $valid = false;
        }
        if(empty($company->getContactDetailsPrivate()->getZip())){
            $this->addError(LocalizationUtility::translate('missingZip', 'artif_companydatabase'), 'zip');
            $valid = false;
        }
//        if (empty($company->getContactDetailsPrivate()->getSalutation())) {
//            $this->addError(LocalizationUtility::translate('missingTitle', 'artif_companydatabase'), 'salutation');
//            $valid = false;
//        }
        if (!\TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($company->getContactDetailsPrivate()->getEmail())) {
            $this->addError(LocalizationUtility::translate('missingEmail', 'artif_companydatabase'), 'email');
            $valid = false;
        }
//        if(!preg_match("/^([0-9]{5})(-[0-9]{4})?$/i",$company->getContactDetailsPrivate()->getZip())){
//            $this->addError(LocalizationUtility::translate('invalidZip', 'artif_companydatabase'), 'zip');
//            $valid = false;
//        }
        if (empty($company->getContactDetailsPrivate()->getCity())) {
            $this->addError(LocalizationUtility::translate('missingCity', 'artif_companydatabase'), 'city');
            $valid = false;
        }

        // CompanyContactPublic
        if (empty($company->getCompanyContactPublic())) {
            $this->addError(LocalizationUtility::translate('missingCompanyContactPublic', 'artif_companydatabase'), 'companyContactPublic');
            $valid = false;
        }
//        if (empty($company->getCompanyContactPublic()->getSalutation())) {
//            $this->addError(LocalizationUtility::translate('missingTitle', 'artif_companydatabase'), 'salutation');
//            $valid = false;
//        }
        if (!\TYPO3\CMS\Core\Utility\GeneralUtility::validEmail($company->getCompanyContactPublic()->getEmail())) {
            $this->addError(LocalizationUtility::translate('missingEmail', 'artif_companydatabase'), 'email');
            $valid = false;
        }
//        if(!preg_match("/^([0-9]{5})(-[0-9]{4})?$/i",$company->getCompanyContactPublic()->getZip())){
//            $this->addError(LocalizationUtility::translate('invalidZip', 'artif_companydatabase'), 'zip');
//            $valid = false;
//        }
//        if(empty($company->getCompanyContactPublic()->getName())){
//            $this->addError(LocalizationUtility::translate('missingName', 'artif_companydatabase'), 'name');
//            $valid = false;
//        }
//        if(empty($company->getCompanyContactPublic()->getFirstName())){
//            $this->addError(LocalizationUtility::translate('missingFirstName', 'artif_companydatabase'), 'firstName');
//            $valid = false;
//        }
        if(empty($company->getCompanyContactPublic()->getZip())){
            $this->addError(LocalizationUtility::translate('missingZip', 'artif_companydatabase'), 'zip');
            $valid = false;
        }
        if (empty($company->getCompanyContactPublic()->getPhone())) {
            $this->addError(LocalizationUtility::translate('missingPhone', 'artif_companydatabase'), 'phone');
            $valid = false;
        }
        if (empty($company->getCompanyContactPublic()->getStreet())) {
            $this->addError(LocalizationUtility::translate('missingStreet', 'artif_companydatabase'), 'street');
            $valid = false;
        }
        if (empty($company->getCompanyContactPublic()->getCity())) {
            $this->addError(LocalizationUtility::translate('missingCity', 'artif_companydatabase'), 'city');
            $valid = false;
        }
        if (!isset($arguments[$parameter]['focuses']) || empty($arguments[$parameter]['focuses'])) {
            $this->addError(LocalizationUtility::translate('missingFocus', 'artif_companydatabase'), 'focuses');
            $valid = false;
        }
        if (count($arguments[$parameter]['focuses']) > 3) {
            $this->addError(LocalizationUtility::translate('tooMuchFocuses', 'artif_companydatabase'), 'focuses');
            $valid = false;
        }

//        if(empty($company->getMobile())){
//            $this->addError('Mobile  not set.', 'mobile');
//            $valid=false;
//        }


        if (isset($arguments['edit']) && $arguments['edit'] == 'edit')
            return $valid;

        if (isset($arguments[$parameter]) && isset($arguments[$parameter]['user']))
            $userData = $arguments[$parameter]['user'];
        else {
            $this->addError(LocalizationUtility::translate('missingUserPassword', 'artif_companydatabase'), 'username');
            $valid = false;
        }
        if (isset($userData['username'])) {
            if (empty($userData['username'])) {
                $this->addError(LocalizationUtility::translate('missingUsername', 'artif_companydatabase'), 'username');
                $valid = false;
            } else if (strlen($userData['username']) < 6) {
                $this->addError(LocalizationUtility::translate('minUser', 'artif_companydatabase'), 'username');
                $valid = false;
            } else if ($this->frontendUserRepository->findByUsername($userData['username'])->count()) {
                $this->addError(LocalizationUtility::translate('existUser', 'artif_companydatabase'), 'username');
                $valid = false;
            }
        }
        if (isset($userData['password']) && strlen($userData['password']) < 6) {
            $this->addError(LocalizationUtility::translate('minPassword', 'artif_companydatabase'), 'password');
            $valid = false;
        } else
            if (!isset($arguments['password2'])) {
                $this->addError(LocalizationUtility::translate('missingPasswordRepeat', 'artif_companydatabase'), 'password2');
                $valid = false;
            } else
                if (isset($arguments['password2']) && isset($userData['password']) && $userData['password'] != $arguments['password2']) {
                    $this->addError(LocalizationUtility::translate('mismatchPassword', 'artif_companydatabase'), 'password');
                    $valid = false;
                }

        return $valid;
    }
}