<?php
namespace  Artif\ArtifCompanydatabase\Authentication;

/**
 * Standard Login of users
 *
 * @author Andrei Todorut <todorutac@gmail.com>
 */
class StandardLogin extends \TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication //implements LoginInterface
{
    /**
     *
     * @param $username
     * @param $password
     * @return boolean
     */
    public function login($username,$password)
    {

        $passwordProcessor = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Saltedpasswords\Salt\PhpassSalt::class);

        $loginData = array(
            'username' => $username, //usernmae
            'password' => $password, //password
            'status' => 'login'
        );

        $this->checkPid = false;
        $info = $this->getAuthInfoArray();
        //$info['db_user']['username_column'] = 'email';

        $user_db = $this->fetchUserRecord($info['db_user'], $loginData['username']);

        $success=false;
        if (\TYPO3\CMS\Saltedpasswords\Utility\SaltedPasswordsUtility::isUsageEnabled('FE')) {
            $objSalt = \TYPO3\CMS\Saltedpasswords\Salt\SaltFactory::getSaltingInstance($user_db['password']);
            if (is_object($objSalt)) {
                $success = $objSalt->checkPassword($password, $user_db['password']);
            }
        }
//        if ($user_db && $passwordProcessor->checkPassword($password, $user_db['password'])) {
        if($success){
            $this->setSession($user_db);
            return true;
        }
        return false;
    }


    public function logout(){
        $GLOBALS['TSFE']->fe_user->logoff();
        $GLOBALS['TSFE']->loginUser = 0;
    }

    private function setSession($user_db)
    {
//        $GLOBALS['TSFE']->fe_user->forceSetCookie = TRUE;
//        $GLOBALS['TSFE']->fe_user->createUserSession($user);
//        $GLOBALS['TSFE']->fe_user->user = $GLOBALS['TSFE']->fe_user->fetchUserSession();
        $GLOBALS['TSFE']->fe_user->createUserSession($user_db);
        // necessary to set session cookie
        $reflection = new \ReflectionClass($GLOBALS['TSFE']->fe_user);
        $setSessionCookieMethod = $reflection->getMethod('setSessionCookie');
        $setSessionCookieMethod->setAccessible(TRUE);
        $setSessionCookieMethod->invoke($GLOBALS['TSFE']->fe_user);
        $GLOBALS['TSFE']->fe_user->user = $user_db;
//        $GLOBALS['TSFE']->fe_user->setKey('ses', 'fe_typo_user', $user_db);
    }
}