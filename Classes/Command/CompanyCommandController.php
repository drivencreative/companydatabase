<?php
namespace Artif\ArtifCompanydatabase\Command;

/***
 *
 * This file is part of the "Company database" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <dradisic@peak-sourcing.com>
 *
 ***/
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * TravelController
 */
class CompanyCommandController extends \TYPO3\CMS\Extbase\Mvc\Controller\CommandController
{

    /**
     * UriBuilder
     *
     * @var \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder
     * @inject
     */
    protected $uriBuilder = NULL;

    /**
     * MailHandler
     *
     * @var \Artif\ArtifCompanydatabase\Service\MailHandler
     * @inject
     */
    protected $mailHandler = null;

    /**
     * companyRepository
     *
     * @var \Artif\ArtifCompanydatabase\Domain\Repository\CompanyRepository
     * @inject
     */
    protected $companyRepository = null;

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository;

    /**
     * action Deletion
     *
     * @return void
     */
    public function deletionCommand() {
        /** @var \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings $querySettings **/
        $querySettings = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings::class);
        $querySettings->setIgnoreEnableFields(TRUE);
        $querySettings->setRespectStoragePage(FALSE);

        $date = new \DateTime();

        $this->companyRepository->setDefaultQuerySettings($querySettings);
        $companies = $this->companyRepository->findByHidden(1);

        if ($companies->count()) {
            foreach ($companies as $company) {
                if ($date->diff($company->getCrdate())->format('%a') > 30) {
                    $this->companyRepository->remove($company);
                }
            }
        }

        $this->frontendUserRepository->setDefaultQuerySettings($querySettings);
        $users = $this->frontendUserRepository->findByDisable(1);

        if ($users->count()) {
            foreach ($users as $user) {
                if ($date->diff($user->getCrdate())->format('%a') > 30) {
                    $this->frontendUserRepository->remove($user);
                }
            }
        }
    }

    /**
     * action remember
     *
     * @return void
     */
    public function rememberCommand() {
//        // read disabled records
//        unset(
//            $GLOBALS['TCA']['tx_artifcompanydatabase_domain_model_company']['ctrl']['enablecolumns']['disabled'],
//            $GLOBALS['TCA']['fe_users']['ctrl']['enablecolumns']['disabled']
//        );

        $typoscript = $this->objectManager->get(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::class)->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
        $plugin = $typoscript['plugin.']['tx_artifcompanydatabase_registration.']['settings.']['scheduler.'];

        $this->initTSFE($plugin['page']);
        $settings = $this->flexFormConfiguration($plugin['plugin']);

        /** @var \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings $querySettings **/
        $querySettings = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings::class);
        if ($plugin['folder']) {
            $querySettings->setStoragePageIds(explode(',', $plugin['folder']));
        } else {
            $querySettings->setRespectStoragePage(FALSE);
        }
        $this->companyRepository->setDefaultQuerySettings($querySettings);
        $companies = $this->companyRepository->findAll();

        $date = new \DateTime();

        if ($companies->count()) {
            /** @var \Artif\ArtifCompanydatabase\Domain\Model\Company $company **/
            foreach ($companies as $company) {
                if ($company->getReminder()) {
                    /** @var \Artif\ArtifCompanydatabase\Domain\Model\Reminder $reminder **/
                    foreach ($company->getReminder() as $reminder) {
                        if ($reminder->getDate() && !$date->diff($reminder->getDate())->format('%a')) {
                            $reminder->setDate(null);
                            $this->companyRepository->update($company);

                            $subject = $content = $link = null;
                            switch ($reminder->getType()) {
                                case 'reminderHalfYear':
                                    $subject = $settings['reminderHalfYear']['subject'];
                                    $content = $settings['reminderHalfYear']['content'];
                                    $link = $this->uriBuilder->setCreateAbsoluteUri(true)->setTargetPageUid($plugin['page'])->setArguments(['tx_artifcompanydatabase_registration' => ['action' => 'edit', 'controller' => 'Company', 'company' => $company]])->buildFrontendUri();
                                    break;
                                case 'reminderOneYear':
                                    $subject = $settings['reminderOneYear']['subject'];
                                    $content = $settings['reminderOneYear']['content'];
                                    $link = $this->uriBuilder->setCreateAbsoluteUri(true)->setTargetPageUid($plugin['page'])->setArguments(['tx_artifcompanydatabase_registration' => ['action' => 'edit', 'controller' => 'Company', 'company' => $company]])->buildFrontendUri();
                                    break;
                                case 'deletion':
                                    $subject = $settings['deletion']['subject'];
                                    $content = $settings['deletion']['content'];
                                    $link = $this->uriBuilder->setCreateAbsoluteUri(true)->setTargetPageUid($plugin['page'])->setArguments(['tx_artifcompanydatabase_registration' => ['action' => 'new', 'controller' => 'Company']])->buildFrontendUri();

                                    $this->companyRepository->remove($company);
                                    break;
                            }
                            if ($subject && $content) {
                                $this->mailHandler
                                    ->setReceiver($company->getContactDetailsPrivate()->getEmail())
                                    ->setSender($settings['senderEmail'])
                                    ->setSubject($subject)
                                    ->setContent($content, [
                                        'company' => $company,
                                        'user' => $company->getCompanyContactPublic(),
                                        'link' => $link
                                    ])
                                    ->send();
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @var string|int $id
     **/
    private function flexFormConfiguration ($id)
    {
        $data = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('pi_flexform', 'tt_content', 'uid='.$id);

//        /** @var \TYPO3\CMS\Core\Database\Query\QueryBuilder $queryBuilder **/
//        $queryBuilder = $this->objectManager->get(\TYPO3\CMS\Core\Database\ConnectionPool::class)->getQueryBuilderForTable('tt_content');
//        $data = $queryBuilder->select('*')
//            ->from('tt_content')
//            ->where($queryBuilder->expr()->eq('uid', $id))
//            ->execute()
//            ->fetch();

        return $this->objectManager->get(\TYPO3\CMS\Extbase\Service\FlexFormService::class)->convertFlexFormContentToArray($data['pi_flexform'])['settings'];
    }

    public function initTSFE($page)
    {
        $GLOBALS['TT'] = $this->objectManager->get(\TYPO3\CMS\Core\TimeTracker\NullTimeTracker::class);
        $GLOBALS['TT']->start();

        $GLOBALS['TSFE'] = $this->objectManager->get(\TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController::class, $GLOBALS['TYPO3_CONF_VARS'], $page, '');
        $GLOBALS['TSFE']->sys_page = $this->objectManager->get(\TYPO3\CMS\Frontend\Page\PageRepository::class);
        $GLOBALS['TSFE']->initFEuser();
        $GLOBALS['TSFE']->initTemplate();
        $GLOBALS['TSFE']->connectToDB();
        $GLOBALS['TSFE']->determineId();
        $GLOBALS['TSFE']->getFromCache();
        $GLOBALS['TSFE']->getConfigArray();
        $GLOBALS['TSFE']->settingLanguage();
        $GLOBALS['TSFE']->settingLocale();
    }
}
