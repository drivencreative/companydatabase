<?php
namespace Artif\ArtifCompanydatabase\System;

/***
 *
 * This file is part of the "Company database" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Dragan Radisic <info@artif.com>
 *
 ***/
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Utility
 */
class Utility {

    /**
     * Create HTML from variables and template path, and send email
     *
     * @param string $receiver
     * @param string $sender
     * @param string $subject
     * @param array $variables
     * @param string $template
     * @param string $attachment
     * @param string $css
     * @param bool $debug
     */
    public static function sendTemplatedMail($receiver, $sender, $subject, $variables, $template, $attachment = NULL, $css = NULL, $debug = FALSE) {
        $messageBody = self::getStandaloneView($variables, $template, $css, $debug);
        self::sendEmail($receiver, $sender, $subject, $messageBody, $attachment);
    }

    /**
     * render fluid standalone view
     *
     * @param array $variables objects passed to view
     * @param string $template file name of partial template
     * @param string $css file path of css
     * @param bool $debug
     *
     * @return string
     */
    public static function getStandaloneView($variables, $template, $css = NULL, $debug = FALSE) {
        $templatePathAndFilename = ExtensionManagementUtility::extPath( 'artif_companydatabase') .'Resources/Private/'. $template;
        $standaloneView = GeneralUtility::makeInstance(\TYPO3\CMS\Fluid\View\StandaloneView::class);
        $standaloneView->getRequest()->setControllerExtensionName('artif_companydatabase');
        $standaloneView->setFormat('html');
        $standaloneView->setTemplateRootPaths([GeneralUtility::getFileAbsFileName('EXT:artif_companydatabase/Resources/Private/Templates')]);
        $standaloneView->setLayoutRootPaths([GeneralUtility::getFileAbsFileName('EXT:artif_companydatabase/Resources/Private/Layouts')]);
        $standaloneView->setPartialRootPaths([GeneralUtility::getFileAbsFileName('EXT:artif_companydatabase/Resources/Private/Partials')]);
        $standaloneView->setTemplatePathAndFilename($templatePathAndFilename);
        $standaloneView->assignMultiple($variables);
        $data = $standaloneView->render();
        $data = self::cssToInline($data, $css);

        if ($debug) { echo $data;die; }

        return $data;
    }

    /**
     * Css to Inline
     *
     * @param string $html
     * @param string $css
     *
     * @return string
     **/
    public static function cssToInline($html, $css = '') {
        if ($css) {
            $cssToInline = new \Artif\ArtifCompanydatabase\System\CssToInline($html, file_get_contents($css));
            $html = $cssToInline->emogrify();
        }
        return $html;
    }

    /**
     * Send mail
     *
     * @param string $receiver
     * @param string $sender
     * @param string $subject
     * @param string $messageBody
     * @param mixed $attachment
     *
     * @return bool
     */
    public static function sendEmail($receiver, $sender, $subject, $messageBody, $attachment = NULL) {
        $message = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class);
        $message->setTo($receiver)->setFrom($sender)->setSubject($subject);
        $message->setBody($messageBody, 'text/html');

        if ($attachment) {
            if (is_array($attachment)) {
                foreach ($attachment as $attach) {
                    $message->attach(\Swift_Attachment::fromPath($attach['url'] ?: $attach));
                }
            } else {
                $message->attach(\Swift_Attachment::fromPath($attachment));
            }
        }

        $message->send();

        return $message->isSent();
    }

    /**
     * Upload file with unique name
     *
     * @param array $file
     * @param string $folder
     *
     * @return string
     */
    public static function upload($file, $folder = 'uploads/media/') {
        self::createFolder($folder);

        $fileName = self::uniqueName($file['name'], $folder);
        \TYPO3\CMS\Core\Utility\GeneralUtility::upload_copy_move($file['tmp_name'], $fileName);
        return basename($fileName);
    }

    /**
     * Unique file name
     *
     * @param array $file
     * @param string $folder
     *
     * @return string
     */
    public static function uniqueName($file, $folder = 'uploads/media/') {
        $basicFile = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Utility\File\BasicFileUtility::class);
        return $basicFile->getUniqueName(str_replace([' ', ':'], '_', $file), \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($folder));
    }

    /**
     * Create folder if don't already exists
     *
     * @param string $folder
     *
     * @return void
     **/
    public static function createFolder($folder) {
        $uploadDir = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($folder);
        if(!is_dir($uploadDir)){
            \TYPO3\CMS\Core\Utility\GeneralUtility::mkdir_deep($uploadDir);
        }
    }

}
