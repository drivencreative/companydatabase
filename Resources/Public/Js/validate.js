$(document).ready(function () {
    $.validator.addMethod('dimensions', function(value, element, param) {
        var isOptional = this.optional(element);
        if(isOptional) {
            return isOptional;
        }
        if ($(element).data('dimensions')) {
            return $(element).data('dimensions') <= param;
        }
    }, 'maximale Auflösung: 600 x 600px.');

    $.validator.addMethod("filesize", function(value, element, param) {
        var isOptional = this.optional(element), file;
        if(isOptional) {
            return isOptional;
        }
        if ($(element).attr("type") === "file") {
            if (element.files && element.files.length) {
                file = element.files[0];
                return ( file.size && file.size <= param );
            }
        }
        return false;
    }, "maximale Dateigröße: 500 kb");

    $.validator.addMethod("maxselected", function (value, element, param) {
    	return value.length && value.length <= param;
    }, "max. 3 auswählbar");

    $.validator.addMethod("minselected", function (value, element, param) {
        if (typeof value == "string") {
            return value && (value != "" || value > 0);
        }
    	return value.length && value.length <= param;
    }, "bitte eine auswählen");

	validate('.validate');
	function validate(el) {
		var validator = $(el).validate({
            errorPlacement: function(error, element) {
                if (element.attr("name") == "tx_artifcompanydatabase_registration[newCompany][logo]" ) {
                    error.insertAfter(element);
                }
                if (element.attr("name") == "tx_artifcompanydatabase_registration[newCompany][focuses][]" ) {
                    error.insertAfter(element.next());
                }
                if (element.attr("name") == "tx_artifcompanydatabase_registration[newCompany][sectors][0]" ) {
                    element.parents('.form-group').append(error);
                }
                if (element.attr("name") == "tx_artifcompanydatabase_registration[newCompany][user][username]" ) {
                    error.insertAfter(element);
                }
            },
			highlight: function (element) {
				$(element).addClass('f3-form-error');
                if ($(element).is('select')) {
                    $(element).parents('.form-group').addClass('f3-form-error');
                }
			},
			unhighlight: function (element) {
				$(element).removeClass('f3-form-error');
                if ($(element).is('select')) {
                    $(element).parents('.form-group').removeClass('f3-form-error');
                }
			}
		});
		$('select').on('change', function () {
			if (validator) validator.element($(this));
		});
        $('input[type=file]').change(function(e) {
            window.URL = window.URL || window.webkitURL;
            var img = new Image(), fileInput = $(this), file = this.files[0];
            img.src = window.URL.createObjectURL(file);
            img.onload = function () {
                var width = img.naturalWidth, height = img.naturalHeight;
                window.URL.revokeObjectURL(img.src);
                fileInput.data('dimensions', width > height ? width : height);
                if (validator) validator.element(fileInput);
            }
        });
    }
});