
plugin.tx_artifcompanydatabase_registration {
    view {
        # cat=plugin.tx_artifcompanydatabase_registration/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:artif_companydatabase/Resources/Private/Templates/
        # cat=plugin.tx_artifcompanydatabase_registration/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:artif_companydatabase/Resources/Private/Partials/
        # cat=plugin.tx_artifcompanydatabase_registration/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:artif_companydatabase/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_artifcompanydatabase_registration//a; type=string; label=Default storage PID
        #storagePid =
    }
}

plugin.tx_artifcompanydatabase_search {
    view {
        # cat=plugin.tx_artifcompanydatabase_search/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:artif_companydatabase/Resources/Private/Templates/
        # cat=plugin.tx_artifcompanydatabase_search/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:artif_companydatabase/Resources/Private/Partials/
        # cat=plugin.tx_artifcompanydatabase_search/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:artif_companydatabase/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_artifcompanydatabase_search//a; type=string; label=Default storage PID
        # storagePid =
    }
}
