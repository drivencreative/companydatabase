
plugin.tx_artifcompanydatabase_registration {
    view {
        templateRootPaths.0 = EXT:artif_companydatabase/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_artifcompanydatabase_registration.view.templateRootPath}
        partialRootPaths.0 = EXT:artif_companydatabase/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_artifcompanydatabase_registration.view.partialRootPath}
        layoutRootPaths.0 = EXT:artif_companydatabase/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_artifcompanydatabase_registration.view.layoutRootPath}
    }
    persistence {
        #storagePid = {$plugin.tx_artifcompanydatabase_registration.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
    settings {
        imagesFolder = 1:/user_upload/
        scheduler {
            page = 67
            plugin = 339
            folder = 68
        }
    }
}

plugin.tx_artifcompanydatabase_search {
    view {
        templateRootPaths.0 = EXT:artif_companydatabase/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_artifcompanydatabase_search.view.templateRootPath}
        partialRootPaths.0 = EXT:artif_companydatabase/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_artifcompanydatabase_search.view.partialRootPath}
        layoutRootPaths.0 = EXT:artif_companydatabase/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_artifcompanydatabase_search.view.layoutRootPath}
    }
    persistence {
        #storagePid = {$plugin.tx_artifcompanydatabase_search.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_artifcompanydatabase._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error, select.f3-form-error, .f3-form-error .ms-choice, .f3-form-error .jcf-select {
        background-color:#FF9F9F !important;
        border: 1px #FF0000 solid !important;
    }

    .form-check-input.f3-form-error {
        border-color: #FF0000;
        box-shadow: inset 1px 1px 0 #fff, 0 0 4px #FF0000;
    }

    .tx-artif-companydatabase table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-artif-companydatabase table th {
        font-weight:bold;
    }

    .tx-artif-companydatabase table td {
        vertical-align:top;
    }

    .typo3-messages .message-error, label.error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }

    .tx-artif-companydatabase input[type="checkbox"], .tx-artif-companydatabase input[type="radio"], .tx-artif-companydatabase input[type="submit"] {
        width: auto;
        margin-right: 10px;
        margin-bottom: 8px;
    }
)

page.includeCSS{
    jcf = EXT:artif_companydatabase/Resources/Public/Js/jcf/css/theme-minimal/jcf.css
    multiple-select = EXT:artif_companydatabase/Resources/Public/Styles/multiple-select.css
}
page.includeJSFooter{
    jquery = EXT:artif_companydatabase/Resources/Public/Js/jquery.min.js
    jcf = EXT:artif_companydatabase/Resources/Public/Js/jcf/js/jcf.js
    jcf_select = EXT:artif_companydatabase/Resources/Public/Js/jcf/js/jcf.select.js
    jcf_file = EXT:artif_companydatabase/Resources/Public/Js/jcf/js/jcf.file.js
    jquery_validate = EXT:artif_companydatabase/Resources/Public/Js/jquery-validation-1.17.0/dist/jquery.validate.js
    jquery_validate_de = EXT:artif_companydatabase/Resources/Public/Js/jquery-validation-1.17.0/dist/localization/messages_de.min.js
    additional-methods = EXT:artif_companydatabase/Resources/Public/Js/jquery-validation-1.17.0/dist/additional-methods.js
    multiple_select = EXT:artif_companydatabase/Resources/Public/multiselect/multiple-select.js
    validate = EXT:artif_companydatabase/Resources/Public/Js/validate.js
    custom = EXT:artif_companydatabase/Resources/Public/Js/custom.js
}