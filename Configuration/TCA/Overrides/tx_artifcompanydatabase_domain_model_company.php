<?php
defined('TYPO3_MODE') || die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
   'artif_companydatabase',
   'tx_artifcompanydatabase_domain_model_company'
);
