<?php
// include FlexForm
$pluginSignature = 'artifcompanydatabase_registration';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:artif_companydatabase/Configuration/FlexForms/ff_registration.xml'
);