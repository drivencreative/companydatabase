<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_reminder',
        'label' => 'type',
        'label_alt' => 'date',
        'label_alt_force' => '1',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'delete' => 'deleted',
        'hideTable' => '1',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'searchFields' => 'type, date',
        'iconfile' => 'EXT:artif_companydatabase/Resources/Public/Icons/tx_artifcompanydatabase_domain_model_reminder.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'hidden, type, date',
    ],
    'types' => [
        '1' => ['showitem' => 'hidden, type, date'],
    ],
    'columns' => [
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],

        'type' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_reminder.type',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_reminder.type.reminderHalfYear','reminderHalfYear'],
                    ['LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_reminder.type.reminderOneYear','reminderOneYear'],
                    ['LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_reminder.type.deletion','deletion'],
                ],
            ],
        ],
        'date' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_reminder.date',
            'config' => [
                'behaviour' => ['allowLanguageSynchronization' => true],
                'type' => 'input',
                'size' => 13,
                'eval' => 'date',
                'default' => 0,
            ],
        ],

        'company' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];
