<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_contact',
        'label' => 'company_name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'company_name,company_description,street,zip,city,homepage,photo,logo,salutation,first_name,name,phone,mobile,email,focuses,sectors,user',
        'iconfile' => 'EXT:artif_companydatabase/Resources/Public/Icons/tx_artifcompanydatabase_domain_model_company.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, street, zip, city, salutation, first_name, name, phone, mobile, email',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, status, cdate, salutation, first_name, name, phone, mobile, email, street, zip, city, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_artifcompanydatabase_domain_model_company',
                'foreign_table_where' => 'AND tx_artifcompanydatabase_domain_model_company.pid=###CURRENT_PID### AND tx_artifcompanydatabase_domain_model_company.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
//            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',

            'config' => [
                'behaviour' => ['allowLanguageSynchronization' => true],
                'renderType' => 'inputDateTime',
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
//            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'behaviour' => ['allowLanguageSynchronization' => true],
                'renderType' => 'inputDateTime',
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'cdate' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.cdate',
            'config' => [
                'readOnly' => true,
                'behaviour' => ['allowLanguageSynchronization' => true],
                'renderType' => 'inputDateTime',
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],

        'street' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.street',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'zip' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.zip',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'city' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.city',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],

        'salutation' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.salutation',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'first_name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.first_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'phone' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.phone',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'mobile' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.mobile',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'email' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],

        'company' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];
