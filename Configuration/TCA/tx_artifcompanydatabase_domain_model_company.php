<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company',
        'label' => 'company_name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'company_name,company_description,street,zip,city,homepage,photo,logo,salutation,first_name,name,phone,mobile,email,focuses,sectors,other,user',
        'iconfile' => 'EXT:artif_companydatabase/Resources/Public/Icons/tx_artifcompanydatabase_domain_model_company.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, terms, company_name, company_description, street, zip, city, homepage, photo, logo, salutation, first_name, name, phone, mobile, email, focuses, sectors, other, user, company_contact_public, contact_details_private',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, terms, status, cdate, user, --div--;LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tabs.company, company_name, profile, homepage, sectors, other, focuses, company_contact_public, contact_details_private, --div--;LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tabs.description, company_description,  photo, logo, --div--;LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tabs.reminder, reminder, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_artifcompanydatabase_domain_model_company',
                'foreign_table_where' => 'AND tx_artifcompanydatabase_domain_model_company.pid=###CURRENT_PID### AND tx_artifcompanydatabase_domain_model_company.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
//            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',

            'config' => [
                'behaviour' => ['allowLanguageSynchronization' => true],
                'renderType' => 'inputDateTime',
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
//            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'behaviour' => ['allowLanguageSynchronization' => true],
                'renderType' => 'inputDateTime',
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'cdate' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.cdate',
            'config' => [
                'readOnly' => true,
                'behaviour' => ['allowLanguageSynchronization' => true],
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],

        'status' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.status',
            'config' => [
//                'readOnly' => true,
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.status.0',0],
                    ['LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.status.1',1],
                    ['LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.status.2',2],
                    ['LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.status.3',3],
                ],
//                'itemsProcFunc' => 'Artif\ArtifCompanydatabase\Controller\CompanyController->updateStatus',
            ],
        ],

        'company_name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.company_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'profile' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.profile',
            'config' => [
                'type' => 'text',
            ],
        ],
        'company_description' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.company_description',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]
        ],
        'street' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.street',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'zip' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.zip',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'city' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.city',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'homepage' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.homepage',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'photo' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.photo',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'photo',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'photo',
                        'tablenames' => 'tx_artifcompanydatabase_domain_model_company',
                        'table_local' => 'sys_file',
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 1
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
        ],
        'logo' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.logo',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'logo',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'logo',
                        'tablenames' => 'tx_artifcompanydatabase_domain_model_company',
                        'table_local' => 'sys_file',
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 1
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
        ],
        'salutation' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.salutation',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'first_name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.first_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'phone' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.phone',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'mobile' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.mobile',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'email' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'focuses' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.focuses',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',

                'foreign_table' => 'tx_artifcompanydatabase_domain_model_focus',
                'MM' => 'tx_artifcompanydatabase_company_focus_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'minitems' =>1,
                'multiple' => 1,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => false,
                    ],
                ],
            ],

        ],
        'sectors' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.sectors',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_artifcompanydatabase_domain_model_sector',
                'MM' => 'tx_artifcompanydatabase_company_sector_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'minitems' =>1,
                'multiple' => 1,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => false,
                    ],
                ],
            ],

        ],
        'other' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.other',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'user' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.user',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'fe_users',
                'minitems' => 1,
                'maxitems' => 1,
                'multiple' => 0,
                'appearance' => [
                    'collapseAll' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],
        ],
        'company_contact_public' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.company_contact_public',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_artifcompanydatabase_domain_model_contact',
                'foreign_field' => 'company',
                'foreign_match_fields' => [
                    'fieldname' => 'company_contact_public'
                ],
                'maxitems' => 1,
                'appearance' => [
                    'collapseAll' => 1,
                    'expandSingle' => 1,
                ],
            ],
        ],
        'contact_details_private' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.contact_details_private',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_artifcompanydatabase_domain_model_contact',
                'foreign_field' => 'company',
                'foreign_match_fields' => [
                    'fieldname' => 'contact_details_private'
                ],
                'maxitems' => 1,
                'appearance' => [
                    'collapseAll' => 1,
                    'expandSingle' => 1,
                ],
            ],
        ],
        'reminder' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artifcompanydatabase_domain_model_company.reminder',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_artifcompanydatabase_domain_model_reminder',
                'foreign_field' => 'company',
                'maxitems' => 99,
                'appearance' => [
                    'collapseAll' => 1,
                    'expandSingle' => 1,
                ],
            ],
        ],
        'terms' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang.xlf:tx_artifcompanydatabase_domain_model_company.term',
            'config' => [
                'type' => 'check',
            ],
        ]
    ],
];
