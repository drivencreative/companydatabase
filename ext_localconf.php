<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {
        $GLOBALS['PAGES_TYPES']['default']['allowedTables'].=',fe_users,fe_groups';
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Artif.ArtifCompanydatabase',
            'Registration',
            [
                'Company' => 'index, login, logout, list, show, confirmation, new, create, edit, update, delete, adminReview, manageCompany, switcher'
            ],
            // non-cacheable actions
            [
                'Company' => 'login, list, show, logout, create, edit, update, delete, adminReview, manageCompany, switcher'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Artif.ArtifCompanydatabase',
            'Search',
            [
                'Company' => 'list, show, logout'
            ],
            // non-cacheable actions
            [
                'Company' => 'list, logout'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerTypeConverter(Artif\ArtifCompanydatabase\Property\TypeConverter\UploadedFileReferenceConverter::class);
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerTypeConverter(Artif\ArtifCompanydatabase\Property\TypeConverter\ObjectStorageConverter::class);

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins {
                    elements {
                        registration {
                            iconIdentifier = artif_companydatabase-plugin-registration
                            title = LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artif_companydatabase_registration.name
                            description = LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artif_companydatabase_registration.description
                            tt_content_defValues {
                                CType = list
                                list_type = artifcompanydatabase_registration
                            }
                        }
                        search {
                            iconIdentifier = artif_companydatabase-plugin-search
                            title = LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artif_companydatabase_search.name
                            description = LLL:EXT:artif_companydatabase/Resources/Private/Language/locallang_db.xlf:tx_artif_companydatabase_search.description
                            tt_content_defValues {
                                CType = list
                                list_type = artifcompanydatabase_search
                            }
                        }
                    }
                    show = *
                }
           }'
        );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
        $iconRegistry->registerIcon(
            'artif_companydatabase-plugin-registration',
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:artif_companydatabase/Resources/Public/Icons/user_plugin_registration.svg']
        );

        $iconRegistry->registerIcon(
            'artif_companydatabase-plugin-search',
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:artif_companydatabase/Resources/Public/Icons/user_plugin_search.svg']
        );

        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = \Artif\ArtifCompanydatabase\Command\CompanyCommandController::class;
    }
);
