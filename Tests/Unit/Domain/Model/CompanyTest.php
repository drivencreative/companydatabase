<?php
namespace Artif\ArtifCompanydatabase\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Dragan Radisic <info@artif.com>
 */
class CompanyTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Artif\ArtifCompanydatabase\Domain\Model\Company
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Artif\ArtifCompanydatabase\Domain\Model\Company();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getCompanyNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getCompanyName()
        );
    }

    /**
     * @test
     */
    public function setCompanyNameForStringSetsCompanyName()
    {
        $this->subject->setCompanyName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'companyName',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCompanyDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getCompanyDescription()
        );
    }

    /**
     * @test
     */
    public function setCompanyDescriptionForStringSetsCompanyDescription()
    {
        $this->subject->setCompanyDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'companyDescription',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStreetReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getStreet()
        );
    }

    /**
     * @test
     */
    public function setStreetForStringSetsStreet()
    {
        $this->subject->setStreet('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'street',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getZipReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getZip()
        );
    }

    /**
     * @test
     */
    public function setZipForStringSetsZip()
    {
        $this->subject->setZip('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'zip',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCityReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getCity()
        );
    }

    /**
     * @test
     */
    public function setCityForStringSetsCity()
    {
        $this->subject->setCity('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'city',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getHomepageReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getHomepage()
        );
    }

    /**
     * @test
     */
    public function setHomepageForStringSetsHomepage()
    {
        $this->subject->setHomepage('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'homepage',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPhotoReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getPhoto()
        );
    }

    /**
     * @test
     */
    public function setPhotoForFileReferenceSetsPhoto()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setPhoto($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'photo',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLogoReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getLogo()
        );
    }

    /**
     * @test
     */
    public function setLogoForFileReferenceSetsLogo()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setLogo($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'logo',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSalutationReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getSalutation()
        );
    }

    /**
     * @test
     */
    public function setSalutationForStringSetsSalutation()
    {
        $this->subject->setSalutation('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'salutation',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFirstNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFirstName()
        );
    }

    /**
     * @test
     */
    public function setFirstNameForStringSetsFirstName()
    {
        $this->subject->setFirstName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'firstName',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName()
    {
        $this->subject->setName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'name',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPhoneReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getPhone()
        );
    }

    /**
     * @test
     */
    public function setPhoneForStringSetsPhone()
    {
        $this->subject->setPhone('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'phone',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMobileReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getMobile()
        );
    }

    /**
     * @test
     */
    public function setMobileForStringSetsMobile()
    {
        $this->subject->setMobile('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'mobile',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEmailReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getEmail()
        );
    }

    /**
     * @test
     */
    public function setEmailForStringSetsEmail()
    {
        $this->subject->setEmail('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'email',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFocusesReturnsInitialValueForFocus()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getFocuses()
        );
    }

    /**
     * @test
     */
    public function setFocusesForObjectStorageContainingFocusSetsFocuses()
    {
        $focus = new \Artif\ArtifCompanydatabase\Domain\Model\Focus();
        $objectStorageHoldingExactlyOneFocuses = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneFocuses->attach($focus);
        $this->subject->setFocuses($objectStorageHoldingExactlyOneFocuses);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneFocuses,
            'focuses',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addFocusToObjectStorageHoldingFocuses()
    {
        $focus = new \Artif\ArtifCompanydatabase\Domain\Model\Focus();
        $focusesObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $focusesObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($focus));
        $this->inject($this->subject, 'focuses', $focusesObjectStorageMock);

        $this->subject->addFocus($focus);
    }

    /**
     * @test
     */
    public function removeFocusFromObjectStorageHoldingFocuses()
    {
        $focus = new \Artif\ArtifCompanydatabase\Domain\Model\Focus();
        $focusesObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $focusesObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($focus));
        $this->inject($this->subject, 'focuses', $focusesObjectStorageMock);

        $this->subject->removeFocus($focus);
    }

    /**
     * @test
     */
    public function getSectorsReturnsInitialValueForSector()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getSectors()
        );
    }

    /**
     * @test
     */
    public function setSectorsForObjectStorageContainingSectorSetsSectors()
    {
        $sector = new \Artif\ArtifCompanydatabase\Domain\Model\Sector();
        $objectStorageHoldingExactlyOneSectors = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneSectors->attach($sector);
        $this->subject->setSectors($objectStorageHoldingExactlyOneSectors);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneSectors,
            'sectors',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addSectorToObjectStorageHoldingSectors()
    {
        $sector = new \Artif\ArtifCompanydatabase\Domain\Model\Sector();
        $sectorsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $sectorsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($sector));
        $this->inject($this->subject, 'sectors', $sectorsObjectStorageMock);

        $this->subject->addSector($sector);
    }

    /**
     * @test
     */
    public function removeSectorFromObjectStorageHoldingSectors()
    {
        $sector = new \Artif\ArtifCompanydatabase\Domain\Model\Sector();
        $sectorsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $sectorsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($sector));
        $this->inject($this->subject, 'sectors', $sectorsObjectStorageMock);

        $this->subject->removeSector($sector);
    }

    /**
     * @test
     */
    public function getUserReturnsInitialValueForFrontendUser()
    {
    }

    /**
     * @test
     */
    public function setUserForFrontendUserSetsUser()
    {
    }
}
