<?php
namespace Artif\ArtifCompanydatabase\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Dragan Radisic <info@artif.com>
 */
class FocusTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Artif\ArtifCompanydatabase\Domain\Model\Focus
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Artif\ArtifCompanydatabase\Domain\Model\Focus();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }
}
