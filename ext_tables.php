<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Artif.ArtifCompanydatabase',
            'Registration',
            'Company registration'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Artif.ArtifCompanydatabase',
            'Search',
            'Company search'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('artif_companydatabase', 'Configuration/TypoScript', 'Company database');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_artifcompanydatabase_domain_model_company', 'EXT:artif_companydatabase/Resources/Private/Language/locallang_csh_tx_artifcompanydatabase_domain_model_company.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_artifcompanydatabase_domain_model_company');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_artifcompanydatabase_domain_model_focus', 'EXT:artif_companydatabase/Resources/Private/Language/locallang_csh_tx_artifcompanydatabase_domain_model_focus.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_artifcompanydatabase_domain_model_focus');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_artifcompanydatabase_domain_model_sector', 'EXT:artif_companydatabase/Resources/Private/Language/locallang_csh_tx_artifcompanydatabase_domain_model_sector.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_artifcompanydatabase_domain_model_sector');

    }
);
