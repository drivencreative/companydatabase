config.tx_extbase {
    persistence {
        # Enable this if you need the reference index to be updated
        # updateReferenceIndex = 1
        classes {
            TYPO3\CMS\Extbase\Domain\Model\FrontendUser {
                subclasses {
                    0 = Artif\ArtifCompanydatabase\Domain\Model\FrontendUser
                }
            }
            Artif\ArtifCompanydatabase\Domain\Model\FrontendUser {
                mapping {
                    tableName = fe_users
                    recordType = 0
                    columns {
                        crdate.mapOnProperty = crdate
                    }
                }
            }

            Artif\ArtifCompanydatabase\Domain\Model\FileReference {
                mapping {
                    tableName = sys_file_reference
                    columns {
                        uid_local.mapOnProperty = originalFileIdentifier
                    }
                }
            }

            Artif\ArtifCompanydatabase\Domain\Model\Company {
                mapping {
                    tableName = tx_artifcompanydatabase_domain_model_company
                    columns {
                        crdate.mapOnProperty = crdate
                        tstamp.mapOnProperty = tstamp
                    }
                }
            }
        }
    }
    objects {
        TYPO3\CMS\Extbase\Domain\Model\FileReference.className = Artif\ArtifCompanydatabase\Domain\Model\FileReference
    }
}